﻿using Autofac;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataAccessDependencies
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register((c) => new InMemoryRepository<Employee>(FakeDataFactory.Employees))
                .As<IRepository<Employee>>().InstancePerLifetimeScope();
            builder.Register((c) => new InMemoryRepository<Role>(FakeDataFactory.Roles))
                .As<IRepository<Role>>().InstancePerLifetimeScope();
        }
    }
}